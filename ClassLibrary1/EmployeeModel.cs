﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class EmployeeModel
    {
     
        public int id { get; set; }

        public string Name { get; set; }


        public int EmployeeNumber { get; set; }


        public int Age { get; set; }

        public char Gender { get; set; }


        public string Hobbies { get; set; }

        public List<EmployeeModel> ShowallEmployees { get; set; }
    }

}  
