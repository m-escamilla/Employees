﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClassLibrary1;
using DataAccess;
using System.Data;
using System.Data.SqlClient;

namespace EmployeesManage.Controllers
{
  

    public class EmployeeController : Controller
    {
        string connectionString = @"Data Source=.; Initial Catalog=employees; Integrated Security=True";

        public ActionResult Index()
        {
            DataTable dtblEmployees = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT * FROM MainEmployee WHERE IsActive=1", sqlCon);
                sqlDa.Fill(dtblEmployees);
            }
            return View(dtblEmployees);
        }

        [HttpGet]
        public ActionResult InsertEmployee()
        {
            return View();
        }
        [HttpPost]
        public ActionResult InsertEmployee(EmployeeModel objEmployee)
        {

            if (ModelState.IsValid)
            {
                DataAccessLayer objDB = new DataAccessLayer();
                string result = objDB.InsertData(objEmployee);
                TempData["result1"] = result;
                ModelState.Clear();
                return RedirectToAction("InsertEmployee");
            }

            else
            {
                ModelState.AddModelError("", "Error in saving data");
                return View();
            }
        }


        [HttpGet]
        public ActionResult Details(string ID)
        {

            EmployeeModel objEmployee = new EmployeeModel();
            DataAccessLayer objDB = new DataAccessLayer();
            return View(objDB.SelectDatabyEN(ID));
        }
        [HttpGet]
        public ActionResult Edit(string EmployeeNumber)
        {
            EmployeeModel objEmployee = new EmployeeModel();
            DataAccessLayer objDB = new DataAccessLayer();
            return View(objDB.SelectDatabyEN(EmployeeNumber));
        }

        [HttpPost]
        public ActionResult Edit(EmployeeModel objEmployee)
        {
            if (ModelState.IsValid)
            {
                DataAccessLayer objDB = new DataAccessLayer();
                string result = objDB.UpdateData(objEmployee);
                TempData["result2"] = result;
                ModelState.Clear();
                return RedirectToAction("ShowAllCustomerDetails");
            }
            else
            {
                ModelState.AddModelError("", "Error in saving data");
                return View();
            }
        }

        [HttpGet]
        public ActionResult Delete(EmployeeModel objEmployee)
        {
            if (ModelState.IsValid)
            {
                DataAccessLayer objDB = new DataAccessLayer();
                string result = objDB.Delete(objEmployee);
               
                TempData["result3"] = result;
                ModelState.Clear();
      
                return RedirectToAction("ShowAllCustomerDetails");
            }
            else
            {
                ModelState.AddModelError("", "Error in saving data");
                return View();
            }
        }
    }
}