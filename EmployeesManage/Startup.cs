﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EmployeesManage.Startup))]
namespace EmployeesManage
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
