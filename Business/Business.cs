﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using System.Data;

namespace Business
{
    public class Business
    {

        private DataAccessLayer M = new DataAccessLayer();

        //Atributos
        public String m_Name { get; set; }
        public int m_EmployeeNumber { get; set; }
        public int m_Age { get; set; }
        public Char m_Gender { get; set; }
        public String m_Hobbies { get; set; }


        public DataTable Listed()
        {
            return M.Listed("List", null);
        }


        public String RegisterEmployee()
        {
            List<Parameters> lst = new List<Parameters>();
            String Message = "";
            try
            {

                lst.Add(new Parameters("@Name", m_Name));
                lst.Add(new Parameters("@EmployeeNumber", m_EmployeeNumber));
                lst.Add(new Parameters("@Age", m_Age));
                lst.Add(new Parameters("@Gender", m_Gender));
                lst.Add(new Parameters("@Hobbies", m_Hobbies));
                lst.Add(new Parameters("@Message", SqlDbType.VarChar, 100));
                M.Procedures("Insert_Employees", ref lst);
                Message = lst[5].m_Value.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Message;
        }

        public String Update()
        {
            List<Parameters> lst = new List<Parameters>();
            String Message = "";
            try
            {
                lst.Add(new Parameters("@Name", m_Name));
                lst.Add(new Parameters("@EmployeeNumber", m_EmployeeNumber));
                lst.Add(new Parameters("@Age", m_Age));
                lst.Add(new Parameters("@Gender", m_Gender));
                lst.Add(new Parameters("@Hobbies", m_Hobbies));
                lst.Add(new Parameters("@Message", SqlDbType.VarChar, 100));
                M.Procedures("Update_Employees", ref lst);
                Message = lst[5].m_Value.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Message;
        }

        public String Delete()
        {
            List<Parameters> lst = new List<Parameters>();
            String Message = "";
            try
            {
                lst.Add(new Parameters("@Name", m_Name));
                lst.Add(new Parameters("@EmployeeNumber", m_EmployeeNumber));
                lst.Add(new Parameters("@Age", m_Age));
                lst.Add(new Parameters("@Gender", m_Gender));
                lst.Add(new Parameters("@Hobbies", m_Hobbies));
                lst.Add(new Parameters("@Message", SqlDbType.VarChar, 100));
                M.Procedures("Delete_Employees", ref lst);
                Message = lst[5].m_Value.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Message;
        }

        public DataTable SearchEmployee(int m_EmployeeNumber)
        {
            throw new NotImplementedException();
        }

        public DataTable SearchEmployee(String objEmp)
        {
            DataTable dt = new DataTable();
            List<Parameters> lst = new List<Parameters>();
            try
            {
                lst.Add(new Parameters("@EmployeeNumber", objEmp));
                dt = M.Listed("Search_Employee", lst);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
