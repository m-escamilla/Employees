﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using ClassLibrary1;
using System.Data;
using System.Configuration;

namespace DataAccess
{
    public class DataAccessLayer
    {

        public SqlConnection con = new SqlConnection("Server=.;DataBase=employees;Integrated Security=true");

        public string InsertData(EmployeeModel objemp)
        {

            string result = "";
            try
            {

                SqlCommand cmd = new SqlCommand("Insert_Employees", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name", objemp.Name);
                cmd.Parameters.AddWithValue("@EmployeeNumber", objemp.EmployeeNumber);
                cmd.Parameters.AddWithValue("@Age", objemp.Age);
                cmd.Parameters.AddWithValue("@Gender", objemp.Gender);
                cmd.Parameters.AddWithValue("@Hobbies", objemp.Hobbies);

                con.Open();
                result = cmd.ExecuteScalar().ToString();
                return result;
            }
            catch
            {
                return result = "";
            }

        }
        public string UpdateData(EmployeeModel objemp)
        {
            SqlConnection con = null;
            string result = "";
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["Server =.; DataBase = employees; Integrated Security = true"].ToString());
                SqlCommand cmd = new SqlCommand("SPCRUD", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name", objemp.Name);
                cmd.Parameters.AddWithValue("@EmployeeNumber", objemp.EmployeeNumber);
                cmd.Parameters.AddWithValue("@Age", objemp.Age);
                cmd.Parameters.AddWithValue("@Gender", objemp.Gender);
                cmd.Parameters.AddWithValue("@Hobbies", objemp.Hobbies);
                cmd.Parameters.AddWithValue("@Query", 2);
                con.Open();
                result = cmd.ExecuteScalar().ToString();
                return result;
            }
            catch
            {
                return result = "";
            }
            finally
            {
                con.Close();
            }
        }
        public string Delete(EmployeeModel objemp)
        {
            SqlConnection con = null;
            string result = "";
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["Server =.; DataBase = employees; Integrated Security = true"].ToString());
                SqlCommand cmd = new SqlCommand("SPCRUD", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name", objemp.Name);
                cmd.Parameters.AddWithValue("@EmployeeNumber", objemp.EmployeeNumber);
                cmd.Parameters.AddWithValue("@Age", objemp.Age);
                cmd.Parameters.AddWithValue("@Gender", objemp.Gender);
                cmd.Parameters.AddWithValue("@Hobbies", objemp.Hobbies);
                cmd.Parameters.AddWithValue("@Query", 3);
                con.Open();
                result = cmd.ExecuteScalar().ToString();
                return result;
            }
            catch
            {
                return result = "";
            }
          
        }

        public EmployeeModel SelectDatabyEN(string EmployeeNumber)
        {
            SqlConnection con = null;
            DataSet ds = null;
            EmployeeModel cobj = null;
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["Server =.; DataBase = employees; Integrated Security = true"].ToString());
                SqlCommand cmd = new SqlCommand("SPCRUD", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmployeeNumber", EmployeeNumber);
                cmd.Parameters.AddWithValue("@Name", null);
                cmd.Parameters.AddWithValue("@Age", null);
                cmd.Parameters.AddWithValue("@Gender", null);
                cmd.Parameters.AddWithValue("@Hobbies", null);
                cmd.Parameters.AddWithValue("@Query", 4);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    cobj = new EmployeeModel();
                    cobj.EmployeeNumber = Convert.ToInt32(ds.Tables[0].Rows[i]["EmployeeNumber"].ToString());
                    cobj.Name = ds.Tables[0].Rows[i]["Name"].ToString();
                    cobj.Age = Convert.ToInt32(ds.Tables[0].Rows[i]["Age"].ToString());
                    cobj.Gender = Convert.ToChar(ds.Tables[0].Rows[i]["Gender"].ToString());
                    cobj.Hobbies = ds.Tables[0].Rows[i]["Hobbies"].ToString();

                }
                return cobj;
            }
            catch
            {
                return cobj;
            }
            
        }
    }
}